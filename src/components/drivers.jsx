import React, { Component } from "react";
import { getDrivers } from "../services/driversService";

class Drivers extends Component {
  state = {
    drivers: []
  };

  constructor() {
    super();
  }

  componentDidMount() {
    getDrivers().then(res => this.setState({ drivers: res.data.drivers }));
    // console.log(getDrivers());
  }

  handeDelete = movie => {
    let drivers = [...this.state.drivers];
    //   const index = drivers.indexOf(movie);
    drivers = drivers.filter(m => m !== movie);
    this.setState({ drivers });
  };

  render() {
    const { drivers } = this.state;
    if (drivers.length === 0) return <p>There is no Data!</p>;

    return (
      <React.Fragment>
        <h4>
          Showing {drivers.length} drivers Available on Your Lovely Teathre
        </h4>
        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Phone</th>
              <th>Email</th>
              <th>Suspended</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.state.drivers.map(driver => (
              <tr key={driver.id}>
                <td>{driver.name}</td>
                <td>{driver.phone}</td>
                <td>{driver.email}</td>
                <td>{driver.suspended ? "NO" : "YES"}</td>
                <td>
                  <button
                    hidden={!driver.suspended}
                    onClick={() => this.handeDelete(driver)}
                    className="btn btn-danger btn-sm"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </React.Fragment>
    );
  }
}

export default Drivers;
