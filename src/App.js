import React, { Component } from "react";
import Drivers from "./components/drivers";

class App extends Component {
  render() {
    return (
      <main className="container">
        <Drivers />
      </main>
    );
  }
}

export default App;
