// import * as genresAPI from './fakeGenreService.js';

const movies= [
    {
        _id: 1,
        title: "Terminator",
        genre: {
            id: 1, name: "Action",
        },
        numberInStock: 6,
        dailyRentalRate: 2.5,
        publishDate: "2018-01-03T19:04:28.809Z"
    },
    {
        _id: 2,
        title: "Die Hard",
        genre: {
            id: 1, name: "Action",
        },
        numberInStock: 5,
        dailyRentalRate: 3.5,
        publishDate: "2018-01-03T19:04:28.809Z"
    },
    {
        _id: 3,
        title: "Harry Potter",
        genre: {
            id: 2, name: "Science Fiction",
        },
        numberInStock: 7,
        dailyRentalRate: 5.5,
        publishDate: "2018-01-03T19:04:28.809Z"
    },
    {
        _id: 4,
        title: "InterStellar",
        genre: {
            id: 2, name: "Science Fiction",
        },
        numberInStock: 8,
        dailyRentalRate: 3.5,
        publishDate: "2018-01-03T19:04:28.809Z"
    },
    {
        _id: 5,
        title: "TinkerBell",
        genre: {
            id: 3, name: "Fantasy",
        },
        numberInStock: 6,
        dailyRentalRate: 2.5,
        publishDate: "2018-01-03T19:04:28.809Z"
    },
    {
        _id: 6,
        title: "Peterpan",
        genre: {
            id: 3, name: "Fantasy",
        },
        numberInStock: 6,
        dailyRentalRate: 2.5,
        publishDate: "2018-01-03T19:04:28.809Z"
    }
]

export function getMovies(){
    return movies;
}

export function getMovie(id){
    return movies.find((m)=> m._id === id);
}