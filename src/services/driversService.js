import axios from "axios";

export function getDrivers() {
  return axios.get("./data/drivers.json");
}
